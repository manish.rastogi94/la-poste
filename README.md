# Technical test

#### Setup

- Install python 3.7 & pipenv
- `pipenv install` to install project's requirements

#### Run

- `pipenv shell` to enter virtual environment (loading the variables in .env)
- `flask run`

#### Explore DB

Database is running on SQLite, it can be browsed using "DB Browser for SQLite" for instance

#### Expected work

1. Connect to La Poste API (https://developer.laposte.fr/)
2. Create an endpoint that fetch the status of a given letter on the La Poste API, and update it in the database
3. Create an endpoint that fetch the status of all letters on the La Poste API, and update it in the database
4. Make previous endpoint respond instantly, and launch an asynchronous task that update all the status

There is no need to do a front interface, you can just share a Postman collection to test it.

#### Bonus

- Unit, integration, E2E tests
- Store the status history of each letter in a new table
- Impress us


#### Completion Update:
1. Added the expected work and bonus tasks.
2. Setup virtual env and enter it.
2. Use command `celery -A app.celery worker --loglevel=INFO` to run the celery worker.
3. Need to make sure redis and celery are installed by default
4. Add your LaPoste API Key in config.py. (Use sandboxed one since test.db data is stale.)

Run flask normally using `flask run`