from app import app
from app.models.letter import Letter
from flask import request
from app import db


@app.route("/ping", methods=["GET"])
def ep_ping():
    return "pong", 200


@app.route("/letters", methods=["POST"])
def ep_setup_create_letter():
    '''
        Added some logical way to add a letter (can be used if needed, not thoroughly tested)
    '''
    letter_tracking_number = request.get_json()["tracking_number"]
    # Example of ORM usage (SQLAlchemy)
    letter = Letter(tracking_number=letter_tracking_number)
    db.session.add(letter)
    db.session.commit()
    return f"All done: letter object {letter.id} has been created", 200
