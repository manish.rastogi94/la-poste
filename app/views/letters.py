from app import app, db
from app.lapost_api import get_letter_status, update_letter_status
from app.models.letter import Letter, LetterStatusHistory
from flask import render_template, request


@app.route("/letter/status/<string:tracking_id>", methods=["GET"])
def ep_letter_status(tracking_id):
    '''
        Endpoint to get the status of a letter and update status along with status history
    '''
    letter = Letter.query.filter_by(tracking_number=tracking_id).first()
    if letter is None:
        return render_template("letter_status.html", error="No letter found with tracking id: " + tracking_id)

    try:
        func_status, letter_resp = get_letter_status(tracking_id)

        if func_status:
            letter_status = letter_resp["status"]
            prev_status = letter.status
            letter.status = letter_status

            letter_status_history = LetterStatusHistory.query.filter_by(letter_id=
                letter.id).first()
            if letter_status_history is None:
                letter_status_history = LetterStatusHistory(
                    letter_id=letter.id, status_history=letter_resp["status_history"])
                db.session.add(letter_status_history)
            else:
                letter_status_history.status_history = letter_resp["status_history"]

            db.session.commit()

            return render_template("letter_status.html", tracking_id=tracking_id, message="Previous Status: {} --> New Status: {}".format(prev_status, letter_status))
        else:
            return render_template("letter_status.html", error=letter_resp)
    except Exception as e:
        return render_template("letter_status.html", error=e)


@app.route("/letters/status", methods=["POST"])
def ep_update_multiple_letter_status():
    '''
        Endpoint to get the status of all given letters and update their status along with status history if it exists.
        Happens in background using celery. Returns asynchronously.
    '''
    tracking_ids = request.get_json()["tracking_ids"]
    letters = Letter.query.filter(
        Letter.tracking_number.in_(tracking_ids)).all()

    if len(letters) == 0:
        return render_template("letter_status.html", error="No letters found with tracking ids: " + ",".join(tracking_ids))

    try:
        update_letter_status.delay(tracking_ids)

        return render_template("letter_status.html", tracking_id=",".join(tracking_ids), message="The task has been queued and the latest status of letters will be updated shortly.")
    except Exception as e:
        return render_template("letter_status.html", error=e)


@app.route("/letters/status", methods=["GET"])
def ep_all_letter_status_get():
    '''
        Endpoint to get the representation of all letters in out DB and their respective statuses.
    '''
    letters = Letter.query.all()
    letter_list = []
    for letter in letters:
        letter_list.append({
            "tracking_id": letter.tracking_number,
            "status": letter.status
        })
    return render_template("letter_status.html", letter_info=letter_list)

@app.route("/letter/status_history/<string:tracking_id>", methods=["GET"])
def ep_letter_status_history(tracking_id):
    '''
        Endpoint to get the status history of a letter already present in a DB table.
    '''
    letter = Letter.query.filter_by(tracking_number=tracking_id).first()
    if letter is None:
        return render_template("letter_status.html", error="No letter found with tracking id: " + tracking_id)

    letter_status_history = LetterStatusHistory.query.filter_by(letter_id = letter.id).first()
    if letter_status_history is None:
        return render_template("letter_status.html", error="No status history found for letter with tracking id: " + tracking_id)

    return render_template("letter_status.html", tracking_id=tracking_id, message="Letter Status History", status_history=letter_status_history.status_history)