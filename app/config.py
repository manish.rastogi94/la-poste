import os


class Config:
    SQLALCHEMY_DATABASE_URI = "sqlite:///test.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    ENV_TYPE = "development"
    LAPOSTE_API_KEY = "<add your own API Key(use the sandboxed one as test.db data is stale)>"
    CELERY_BROKER_URL = "redis://localhost:6379/0" #redis for celery broker usage

class ProductionConfig(Config):
    ENV_TYPE = "production"


config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "default": DevelopmentConfig,
}
