import requests

from app import app, celery, db
from app.models.letter import Letter, LetterStatusHistory
API_PATH = "https://api.laposte.fr/suivi/v2/idships/{}?lang=en_GB"


def get_letter_status(tracking_id):
    """
    Get the status of a letter from its tracking number
    """
    # Wanted to use this bit of code but Laposte api didn't respond approproiately to comma separated queries
    # elif len(tracking_list) < 11:
    #     tracking_list = ",".join(tracking_list)
    #     url = API_PATH.format(tracking_list)

    try:
        url = API_PATH.format(tracking_id)
        resp_dict = {}
        response = requests.get(
            url, headers={"X-Okapi-Key": app.config["LAPOSTE_API_KEY"], "accept": "application/json"})
        resp_json = response.json()
        if response.status_code == 200:
            resp_dict["status_history"] = resp_json["shipment"]["event"]
            resp_dict["status"] = resp_json["shipment"]["event"][0]["code"]
            return True, resp_dict
        else:
            return False, "Error in fetching letter - Message:{} Resp Code:{} ".format(resp_json["returnMessage"], response.status_code)
    except Exception as e:
        return False, "Error: {}".format(e)


@celery.task(name="app.tasks.update_letter_status")
def update_letter_status(tracking_ids):
    """
    Update the status of letters from its tracking number as a background task
    """

    letters = Letter.query.filter(
        Letter.tracking_number.in_(tracking_ids)).all()

    try:
        for letter in letters:
            tracking_id = letter.tracking_number
            status, letter_resp = get_letter_status(tracking_id)

            if status:
                letter.status = letter_resp["status"]
                print(letter_resp)
                letter_status_history = LetterStatusHistory.query.filter_by(letter_id=int(letter.id)).first()
                if letter_status_history is None:
                    letter_status_history = LetterStatusHistory(letter_id=int(letter.id), status_history=letter_resp["status_history"])
                    db.session.add(letter_status_history)
                else:
                    letter_status_history.status_history = letter_resp["status_history"]

            db.session.commit()
        return "Successfully done!"
    except Exception as e:
        return False, "Error: {}".format(e)
