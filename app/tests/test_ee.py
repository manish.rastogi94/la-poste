import os, json
import tempfile
import time
import pytest
from app import app as app_test
from app import db
from app.models.letter import Letter
from flask.app import Flask


@pytest.fixture
def client():
    db_fd, db_path = tempfile.mkstemp()
    # In Memory sqlite db
    app_test.config.update(
        TESTING=True,
        SQLALCHEMY_DATABASE_URI="sqlite://"
    )
    with app_test.test_client() as client:
        with app_test.app_context():
            db.init_app(app_test)
            db.create_all()
            db.session.add(Letter(tracking_number='3P11111111110'))
            db.session.add(Letter(tracking_number='3C00638671238'))
            db.session.add(Letter(tracking_number='3C00638619223'))

            db.session.commit()
        yield client

    os.close(db_fd)
    os.unlink(db_path)


def test_all_letter_status(client):
    '''
        Test representation of letters and their status
    '''
    resp = client.get('/letters/status')
    assert resp.status_code == 200

    resp_data = resp.data.decode('utf-8')
    
    assert "Tracking ID" in resp_data
    assert "3P11111111110" in resp_data
    assert "3C00638671238" in resp_data
    assert "3C00638619223" in resp_data
    
def test_letter_status(client):
    '''
        Test the updation of status of a letter 
    '''
    ID_DOESNT_EXIST = '2P11111111110'
    NORMAL_ID = '3P11111111110'
    resp = client.get('/letter/status/'+NORMAL_ID)
    assert resp.status_code == 200

    resp_data = resp.data.decode('utf-8')
    
    assert "Tracking ID" in resp_data
    assert "3P11111111110" in resp_data
    assert "Message: Previous Status: None --&gt; New Status: DI1" in resp_data

    resp = client.get('/letter/status/'+ID_DOESNT_EXIST)
    assert resp.status_code == 200

    resp_data = resp.data.decode('utf-8')
    
    assert "No letter found with tracking id: {}".format(ID_DOESNT_EXIST) in resp_data

def test_letter_status_history(client):
    '''
        Test status history of a letter is getting updated
    '''
    ID_DOESNT_EXIST = '2P11111111110'
    NORMAL_ID = '3P11111111110'
    NO_STATUS_HISTORY = '3C00638671238'

    resp = client.get('/letter/status/'+NORMAL_ID)
    assert resp.status_code == 200

    resp = client.get('/letter/status_history/'+NORMAL_ID)
    assert resp.status_code == 200

    resp_data = resp.data.decode('utf-8')
    
    assert "Tracking ID" in resp_data
    assert "3P11111111110" in resp_data
    assert "Letter Status History" in resp_data

    resp = client.get('/letter/status_history/'+ID_DOESNT_EXIST)
    assert resp.status_code == 200

    resp_data = resp.data.decode('utf-8')
    
    assert "No letter found with tracking id: {}".format(ID_DOESNT_EXIST) in resp_data

    resp = client.get('/letter/status_history/'+NO_STATUS_HISTORY)
    assert resp.status_code == 200

    resp_data = resp.data.decode('utf-8')
    
    assert "No status history found for letter with tracking id: {}".format(NO_STATUS_HISTORY) in resp_data
    

def test_letters_status(client):
    '''
        Test the updation of status of all letters using celery in background
    '''
    ID_DOESNT_EXIST = '2P11111111110'
    ID_DOESNT_EXIST2 = '2P11111111111'
    NORMAL_ID = '3P11111111110'
    NO_STATUS_HISTORY = '3C00638671238'

    mimetype = 'application/json'
    headers = {
        'Content-Type': mimetype,
    }

    post_data = {
        'tracking_ids': [ID_DOESNT_EXIST, ID_DOESNT_EXIST2]
    }

    resp = client.post('/letters/status', data=json.dumps(post_data), headers=headers)
    assert resp.status_code == 200
    resp_data = resp.data.decode('utf-8')
    assert "No letters found with tracking ids: {}".format(','.join([ID_DOESNT_EXIST, ID_DOESNT_EXIST2])) in resp_data
    

    post_data = {
        'tracking_ids': [NORMAL_ID, ID_DOESNT_EXIST, NO_STATUS_HISTORY]
    }

    resp = client.post('/letters/status', data=json.dumps(post_data), headers=headers)
    assert resp.status_code == 200

    resp_data = resp.data.decode('utf-8')
    
    assert "The task has been queued and the latest status of letters will be updated shortly." in resp_data
    time.sleep(10)

    letters = Letter.query.filter(
        Letter.tracking_number.in_(post_data['tracking_ids'])).all()

    assert len(letters) == 2
    for letter in letters:
        assert letter.tracking_number in post_data['tracking_ids']
        assert letter.status in ['DI1', None]

    