from app.lapost_api import get_letter_status

def test_single_letter_status():
    ID_DOESNT_EXIST = '2P11111111110'
    NORMAL_ID = '3P11111111110'

    status_no_exist, resp_no_exist = get_letter_status(ID_DOESNT_EXIST)
    status_normal, resp_normal = get_letter_status(NORMAL_ID)

    assert status_no_exist == False
    assert status_normal == True

    assert "Error" in resp_no_exist
    assert "status" in resp_normal
    assert "status_history" in resp_normal

