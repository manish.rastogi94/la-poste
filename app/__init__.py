import os

from celery.app.base import Celery
from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from app.config import config

app = Flask(__name__)

CORS(app, origins="*", supports_credentials=True)

config_name = os.getenv("FLASK_CONFIG") or "default"
app.config.from_object(config[config_name])

celery = Celery(app.name, broker=app.config["CELERY_BROKER_URL"])

db = SQLAlchemy(app)
migrate = Migrate(app, db)

from .models import *
from .views import *
